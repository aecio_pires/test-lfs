# test-lfs

<!-- TOC -->

- [test-lfs](#test-lfs)
- [Clone a repository](#clone-a-repository)
- [Using LFS in repo Git](#using-lfs-in-repo-git)
  - [Install the Git LFS client locally](#install-the-git-lfs-client-locally)
  - [Set up Git LFS file tracking locally](#set-up-git-lfs-file-tracking-locally)
- [Details of remote repository](#details-of-remote-repository)

<!-- TOC -->


# Clone a repository

1. Clone the repository to your machine with the command follow.

```bash
git clone git@bitbucket.org:aecio_pires/test-lfs.git
cd test-lfs
```

2. Create a branch with the command follow. 

Example:

```bash
git checkout -b <branch name>
```

3. Develop the task.
4. Do commit and push changes to remote repository with the command follow.

Example:

```bash
git push --set-upstream origin <branch name>
```

5. Create Pull Request (PR) to the ``master`` branch, using the notations patterns of the development process.
6. Update the content with the suggestions of the reviewer (if necessary).
7. After your pull request is merged, update and clean your local clone.

```bash
git checkout master
git branch -d <branch name>
```

# Using LFS in repo Git

## Install the Git LFS client locally

Each person who wants to use Git LFS needs to install the client (package ``git-lfs``) on their local machine. They only need to do this once.

Ubuntu 18.04:

```bash
sudo apt -y install git git-lfs
```

Access repository directory and run the command:

```bash
cd test-lfs
git lfs install --local
```

Will be created ``.gitattributes`` file in local repository.

> Attention!!! Only need to do this once.

## Set up Git LFS file tracking locally

Create a binary dir and add file in repository.

```bash
mkdir -p bin
mv cp ~/Downloads/teamviewer_15.9.5_amd64.deb bin/
```

You should set up tracking for each repo that will use Git LFS.

On your local machine:

```bash
git lfs track 'bin/*.deb'
```

View the changes in ``.gitattributes`` file

```bash
cat .gitattributes 
```

Commit and push the changes to ``.gitattributes`` in the usual way:

```bash
git add .gitattributes
git commit -m "add Git LFS to the repo"
git push origin master
```

Now you can add files and they will automatically be handled by LFS:

```bash
git add bin/teamviewer_15.9.5_amd64.deb
git commit -m "Add LFS file"
git push origin master
```

Reference:

* https://support.atlassian.com/bitbucket-cloud/docs/use-git-lfs-with-bitbucket/

# Details of remote repository

* Details of repository:

![1.png](images/1.png "1.png")

> OBS.: The storage limit of Git LFS is 5 GB in account standard https://bitbucket.org/product/pricing.

* Reference of file in Git LFS:
![2.png](images/2.png "2.png")

* Download file of LFS:
![3.png](images/3.png "3.png")

* File in repository:

![4.png](images/4.png "4.png")

* File storaged in LFS:

![5.png](images/5.png "5.png")
